# VIT

A basic CLI tool that is made to run in various CIs, such as the Gitlab CI to help you with your versioning process.
Just download a release of VIT and execute it, when you are in your repository.
If you have already tagged some releases before, it should return something like:

```sh
$ ./vit
0.3.0-alpha-3.13.2 alpha-3 3-extend-parsing
```

It returns in the following format:
```
<long version id> <short version id> <branch name>
```

`<long version id>` can be used as a semver compatible tag for all your builds. See the examples for a demonstration of that.
`<short version id>` can be used for docker images, so you always are on the newest build of a specific branch.
`<branch name>` can just be useful if running in the CI, as on a tagged pipeline on the Gitlab CI it's not trivial to get the branch name, so this can help.

Here are some examples, how it can be used:

## Supported Git branching styles

### Git Flow

```mermaid
sequenceDiagram
    participant m as main
    participant d as develop
    participant f as (feature/)✨
    
    m ->> m: tag
    Note left of m: 1.0.0 <br> stable
    m -) d: checkout
    Note left of d: 1.1.0-alpha.0.0 <br> alpha
    d -) f: checkout
    Note left of f: 1.1.0-alpha-✨.0.0 <br> alpha-✨
    f -->> f: Add ⭐
    Note left of f: 1.1.0-alpha-✨.0.1 <br> alpha-✨
    f -->> f: Add 2
    Note left of f: 1.1.0-alpha-✨.0.2 <br> alpha-✨
    f ->> d: merge
    Note left of d: 1.1.0-alpha.0.3 <br> alpha
    d ->> m: merge
    Note left of m: 1.1.0-beta.0.4 <br> beta
    m ->> m: tag
    Note left of m: 1.1.0 <br> stable

```

### Git Trunk

```mermaid
sequenceDiagram
    participant m as main
    participant f as (feature/)✨
    
    m ->> m: tag
    Note left of m: 1.0.0 <br> stable
    m-) f: checkout
    Note left of f: 1.1.0-alpha-✨.0.0 <br> alpha-✨
    f -->> f: Add ⭐
    Note left of f: 1.1.0-alpha-✨.0.1 <br> alpha-✨
    f -->> f: Add 2
    Note left of f: 1.1.0-alpha-✨.0.2 <br> alpha-✨
    f ->> m: merge
    Note left of m: 1.1.0-beta.0.3 <br> beta
    m ->> m: tag
    Note left of m: 1.1.0 <br> stable

```

### Main only 🙈

```mermaid
sequenceDiagram
    participant m as main
    
    m ->> m: tag
    Note left of m: 1.0.0 <br> stable
    m -->> m: Add ⭐
    Note left of m: 1.1.0-beta.0.1 <br> beta
    m -->> m: Add 2
    Note left of m: 1.1.0-beta.0.2 <br> beta
    m ->> m: tag
    Note left of m: 1.1.0 <br> stable

```

## Getting of feature id

> Note: If the branch starts with `feature/` this will be stripped. So for example both `feature/3-extend-parsing` and `3-extend-parsing` result in the same feature id.

| style name                       | style                       | tag                  | example                 | tag              |
|----------------------------------|-----------------------------|----------------------|-------------------------|------------------|
| Gitlab default                   | `<issue id>-<title>`        | `<issue id>`         | 3-extend-parsing        | 3                |
| Feature branches w/ ID           | `feature/<issue id>`        | `<issue id>`         | feature/3               | 3                |
| Feature branches w/ ID with desc | `feature/<issue id>/<desc>` | `<issue id>`         | feature/3/extendParsing | 3                |
| Feature branches wo/ ID          | `feature/<title>`           | `<title>`            | feature/extendParsing   | extendParsing    |
| Jira                             | `<ticket id>`               | `<ticket id>`        | DEVOPS-3                | DEVOPS-3         |
| Jira with desc                   | `<ticket id>/<desc>`        | `<ticket id>`        | DEVOPS-3/extendParsing  | DEVOPS-3         |

If some other scheme is needed, open a ticket or create a MR.

## Why this format?

### Git Flow example

```mermaid
sequenceDiagram
    participant m as main
    participant d as develop
    participant f as (feature/)✨
    
    m ->> m: tag
    Note left of m: 1.0.0 <br> stable
    m -) d: checkout
    Note left of d: 1.1.0-alpha.0.0 <br> alpha
    d -) f: checkout
    Note left of f: 1.1.0-alpha-✨.0.0 <br> alpha-✨
    f -->> f: Add ⭐
    Note left of f: 1.1.0-alpha-✨.0.1 <br> alpha-✨
    m -->> m: Add hotfix
    Note left of m: 1.1.0-beta-✨.0.1 <br> beta
    m ->> m: tag
    Note left of m: 1.0.1 <br> stable
    f -->> f: Add 2
    Note left of f: 1.1.0-alpha-✨.1.3 <br> alpha-✨
    f ->> d: merge
    Note left of d: 1.1.0-alpha.1.4 <br> alpha
    d ->> m: merge
    Note left of m: 1.1.0-beta.1.5 <br> beta
    m ->> m: tag
    Note left of m: 1.1.0 <br> stable
```
