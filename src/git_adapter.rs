use core::panic;
use git2::{
    BranchType, DescribeFormatOptions, DescribeOptions, FetchOptions, Reference, Repository,
};
use std::borrow::BorrowMut;

pub struct GitAdapter {
    repository: Repository,
}

impl GitAdapter {
    pub fn new(path: &str) -> GitAdapter {
        GitAdapter {
            repository: Self::get_repository(path),
        }
    }

    pub fn get_repository(path: &str) -> Repository {
        match Repository::open(path) {
            Ok(repo) => repo,
            Err(e) => panic!("could not open repository: {}", e),
        }
    }

    pub fn get_git_description(&self) -> String {
        let options = DescribeOptions::new();

        let description = match self.repository.describe(&options) {
            Ok(description) => description,
            Err(_) => {
                self.fetch_remote();
    
                match self.repository.describe(&options) {
                    Ok(description) => description,
                    Err(e) => panic!("failed to describe (no available tags?): {}", e),
                }
            }
        };

        let mut format_options = DescribeFormatOptions::new();
        format_options.abbreviated_size(13);
        format_options.always_use_long_format(true);
        //formatOptions.dirty_suffix("dirty");

        match description.format(Some(format_options).as_ref()) {
            Ok(tag) => tag,
            Err(e) => panic!("failed to describe: {}", e),
        }
    }

    pub fn get_current_branch(&self) -> String {
        let head = match self.repository.head() {
            Ok(head) => head,
            Err(e) => panic!("failed to find head: {}", e),
        };

        if let Some(name) = self.parse_branches(&head) {
            return name;
        }

        self.fetch_remote();

        if let Some(name) = self.parse_branches(&head) {
            return name;
        }

        panic!("could not find branch matching current HEAD!")
    }

    fn fetch_remote(&self) {
        // Try to fetch more information from remote
        // Needed in Gitlab CI when in a Tag pipeline, because branches are not available
        let remotes = match self.repository.remotes() {
            Ok(remotes) => remotes,
            Err(e) => panic!("unable to get enough infos from git repository, so tried to fetch some, but unable to find remotes. Do a `git fetch` manually pls: {}", e)
        };

        let mut remote = self
            .repository
            .find_remote(remotes.get(0).unwrap())
            .unwrap();

        match remote.connect_auth(
            git2::Direction::Fetch,
            Some(git2::RemoteCallbacks::new()),
            None,
        ) {
            Ok(callback) => callback,
            Err(e) => panic!("can't connect to remote: {}", e),
        };

        let refspecs: git2::string_array::StringArray = match remote.fetch_refspecs() {
            Ok(refspecs) => refspecs,
            Err(e) => panic!("unable to fetch refspecs from remote: {}", e),
        };

        let refspec_vec: Vec<&str> = refspecs.into_iter().flatten().collect();

        match remote.fetch(&refspec_vec, Some(FetchOptions::new().borrow_mut()), None) {
            Ok(()) => {}
            Err(e) => panic!("unable to fetch remote repository: {}", e),
        }
    }

    fn parse_branches(&self, head: &Reference) -> Option<String> {
        let branches_remote = match self.repository.branches(Some(BranchType::Remote)) {
            Ok(branches) => branches,
            Err(e) => panic!("unable to find branches: {}", e),
        };

        let branches_local = match self.repository.branches(Some(BranchType::Local)) {
            Ok(branches) => branches,
            Err(e) => panic!("unable to find branches: {}", e),
        };

        let branches = branches_remote.chain(branches_local);

        for branch_tuple in branches {
            let branch = branch_tuple.unwrap().0;
            let name = String::from(
                branch
                    .name()
                    .unwrap()
                    .unwrap()
                    .trim_start_matches("origin/"),
            );
            if branch.into_reference() == *head {
                return Some(name);
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    use git2::{Commit, Error, ObjectType, Oid, Repository, RepositoryInitOptions, Signature};
    use std::env;
    use std::fs::File;
    use std::io::Write;
    use std::path::Path;
    use tempdir::TempDir;

    use super::GitAdapter;

    fn create_repo() -> TempDir {
        let tmp_dir = match TempDir::new("sample-repository") {
            Ok(tmp) => tmp,
            Err(e) => panic!("failed to create tmp directory: {}", e),
        };

        let mut repository_init_options = RepositoryInitOptions::new();
        repository_init_options.initial_head("main");

        let _repo = match Repository::init_opts(tmp_dir.path(), &repository_init_options) {
            Ok(repo) => repo,
            Err(e) => panic!("failed to create repo: {}", e),
        };

        tmp_dir
    }

    #[test]
    fn create_temp_repository() {
        let tmp_dir = create_repo();
        let path = tmp_dir.path();
        assert!(path.exists());
    }

    #[test]
    fn create_file() {
        let tmp_dir = create_repo();
        let file_path = tmp_dir.path().join("amazing-code.py");
        let mut tmp_file = File::create(file_path).unwrap();
        writeln!(tmp_file, "print(\"Hello world\")").unwrap();

        assert!(tmp_file.metadata().is_ok());
    }

    fn create_initial_commit(repo: &Repository, message: &str) -> Result<(), Error> {
        let sig = Signature::now("Benjamin", "thisemaildoesnotexit@localhost")?;

        // Create an empty tree for this commit
        let tree_id = {
            let mut index = repo.index()?;

            // Outside of this example, you could call index.add_path()
            // here to put actual files into the index. For our purposes, we'll
            // leave it empty for now.

            index.write_tree()?
        };

        let tree = repo.find_tree(tree_id)?;

        // Ready to create the initial commit.
        //
        // Normally creating a commit would involve looking up the current HEAD
        // commit and making that be the parent of the initial commit, but here this
        // is the first commit so there will be no parent.
        repo.commit(Some("HEAD"), &sig, &sig, &message, &tree, &[])?;

        Ok(())
    }

    fn add_all(repo: &Repository) -> Result<Oid, git2::Error> {
        let mut index = repo.index()?;
        index.add_all(["*"].iter(), git2::IndexAddOption::DEFAULT, None)?;
        index.write()?;
        index.write_tree()
    }

    fn setup_repo() -> (TempDir, GitAdapter) {
        let tmp_dir = create_repo();
        let path_to_repo = tmp_dir.path().to_str().unwrap();
        let git_adapter = GitAdapter::new(path_to_repo);

        (tmp_dir, git_adapter)
    }

    fn write_test_file(tmp_dir: &TempDir, filename: &str) -> Result<(), std::io::Error> {
        let relative_path = Path::new(&filename);
        let file_path = tmp_dir.path().join(relative_path);
        let mut tmp_file = File::create(&file_path)?;
        writeln!(tmp_file, "print(\"Hello world\")")?;

        Ok(())
    }

    fn find_last_commit(repo: &Repository) -> Result<Commit, git2::Error> {
        let obj = repo.head()?.resolve()?.peel(ObjectType::Commit)?;
        obj.into_commit()
            .map_err(|_| git2::Error::from_str("Couldn't find commit"))
    }

    fn commit(repo: &Repository, oid: &Oid) -> Result<Oid, Error> {
        let sig = Signature::now("Franzi", "thisemaildoesalsonotexist@localhost")?;

        repo.commit(
            Some("HEAD"),
            &sig,
            &sig,
            "This is a commit",
            &repo.find_tree(*oid)?,
            &[&find_last_commit(&repo)?],
        )
    }

    fn setup_repo_with_commit() -> (
        TempDir,
        GitAdapter,
        Result<(), std::io::Error>,
        Result<Oid, Error>,
        Result<(), Error>,
    ) {
        let (tmp_dir, git_adapter) = setup_repo();
        let file_result = write_test_file(&tmp_dir, "main.py");
        let add_result = add_all(&git_adapter.repository);
        let oid = create_initial_commit(&git_adapter.repository, "First commit ✨");
        return (tmp_dir, git_adapter, file_result, add_result, oid);
    }

    fn setup_repo_with_tag() -> (TempDir, GitAdapter) {
        let (tmp_dir, git_adapter, _file_result, _add_result, _oid) = setup_repo_with_commit();
        match write_test_file(&tmp_dir, "cli_parser.py") {
            Ok(val) => val,
            Err(err) => panic!("Could not create test file: {}", err),
        }
        let oid = match add_all(&git_adapter.repository) {
            Ok(val) => val,
            Err(err) => panic!("Could not stage files: {}", err),
        };

        match commit(&git_adapter.repository, &oid) {
            Ok(val) => val,
            Err(err) => panic!("Could not stage files: {}", err),
        };

        match env::set_current_dir(&tmp_dir.path()) {
            Ok(_) => return (tmp_dir, git_adapter),
            Err(err) => panic!("Could not switch working directory: {}", err),
        };
    }

    #[test]
    fn create_initial_commit_test() {
        let (_tmp_dir, git_adapter, file_result, add_result, oid) = setup_repo_with_commit();
        assert_eq!(file_result.is_ok(), true);
        assert_eq!(add_result.is_ok(), true);
        assert_eq!(oid, Ok(()));
        assert_eq!(git_adapter.get_current_branch(), "main");
    }

    // #[test]
    // fn create_initial_commit_test_gitlabci_tag_pipeline() {
    //     let (_tmp_dir, git_adapter, file_result, add_result, oid) = setup_repo_with_commit();
    //     env::set_var("CI", "true");
    //     env::set_var("GITLAB_CI", "true");
    //     env::set_var("CI_COMMIT_REF_NAME", "0.2.0-beta-1");

    //     assert_eq!(file_result.is_ok(), true);
    //     assert_eq!(add_result.is_ok(), true);
    //     assert_eq!(oid, Ok(()));
    //     assert_eq!(git_adapter.get_current_branch(), "main");

    //     env::remove_var("CI");
    //     env::remove_var("GITLAB_CI");
    //     env::remove_var("CI_COMMIT_REF_NAME");
    // }

    #[test]
    fn create_repo_with_two_commits() {
        let (_tmp_dir, git_adapter) = setup_repo_with_tag();
        assert_eq!(git_adapter.get_current_branch(), "main");
    }

    // #[ignore] // TODO: WIP
    // #[test]
    // fn gitlab_get_description() {
    //     let (_tmp_dir, git_adapter) = setup_repo_with_tag();

    //     env::set_var("CI", "true");
    //     env::set_var("GITLAB_CI", "true");
    //     env::set_var("GIT_STRATEGY", "clone");
    //     env::set_var("GIT_DEPTH", "30");

    //     let result = GitAdapter::get_git_description(&git_adapter);
    // }
}
