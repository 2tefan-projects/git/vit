use regex::Regex;

#[derive(Debug)]
pub struct Versionator {
    branch_name: String,
    last_tagged_version: String,
    commits_since_tag: i32,
}

impl Versionator {
    pub fn new(git_descriptor: String, branch_name: String) -> Versionator {
        let parts: Vec<&str> = git_descriptor.split('-').collect();
        let last_tagged_version = parts[0].strip_prefix('v').unwrap_or(parts[0]).to_string();
        let commits_since_tag = match parts[parts.len() - 2].to_string().parse::<i32>() {
            Ok(commits_since_tag) => commits_since_tag,
            Err(e) => panic!("Couldn't parse commits since last tag! {}", e),
        };

        Versionator {
            branch_name,
            last_tagged_version,
            commits_since_tag,
        }
    }

    pub fn get_version_id(&self) -> String {
        let (new_version, last_patch) = Self::get_next_version(self.last_tagged_version.as_str());
        format!(
            "{}-{}.{}.{}",
            new_version,
            self.get_phase(),
            last_patch,
            self.commits_since_tag
        )
    }

    pub fn get_version_id_short(&self) -> String {
        self.get_phase()
    }

    fn get_phase(&self) -> String {
        match self.branch_name.as_str() {
            "main" | "master" => {
                if self.commits_since_tag > 0 {
                    "beta".to_string()
                } else {
                    "stable".to_string()
                }
            }
            "develop" => "alpha".to_string(),
            _ => format!("alpha-{}", Self::get_feature_id(self.branch_name.as_str())),
        }
    }

    fn get_feature_id(raw_branch_name: &str) -> String {
        let branch_name = raw_branch_name.trim_start_matches("feature/");

        let mut re = Regex::new(r"^(?<feat_id>\d+)-.*$").unwrap(); // Gitlab Style
        let cap = re.captures(branch_name);
        if cap.is_some() {
            return cap.unwrap()["feat_id"].to_string();
        }

        re = Regex::new(r"^(?<feat_id>\d+|\w+)(/+.*)*$").unwrap(); // Feature branch
        let cap = re.captures(branch_name);
        if cap.is_some() {
            return cap.unwrap()["feat_id"].to_string();
        }

        re = Regex::new(r"(?<feat_id>\w+-\d)(/+.*)*").unwrap(); // Jira style
        let cap = re.captures(branch_name);
        if cap.is_some() {
            return cap.unwrap()["feat_id"].to_string();
        }

        panic!(
            "Invalid branch name! ({}) See documentation for supported styles!",
            raw_branch_name
        )
    }

    fn get_next_version(old_version: &str) -> (String, String) {
        let parts: Vec<&str> = old_version.split('.').collect();
        let major_version = parts[0].parse::<i32>().unwrap();
        let minor_version = parts[1].parse::<i32>().unwrap() + 1;
        let patch_version = parts[2].parse::<i32>().unwrap();

        (format!("{}.{}.{}", major_version, minor_version, 0), patch_version.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_phase_stable() {
        let versionator =
            Versionator::new("1.0.0-0-g1a9ea6f730a2c".to_string(), "main".to_string());

        assert_eq!(versionator.get_phase(), "stable")
    }

    #[test]
    fn get_phase_stable_master() {
        let versionator =
            Versionator::new("1.0.0-0-g1a9ea6f730a2c".to_string(), "master".to_string());

        assert_eq!(versionator.get_phase(), "stable")
    }

    #[test]
    fn get_phase_beta() {
        let versionator =
            Versionator::new("0.0.0-2-g1a9ea6f730a2c".to_string(), "main".to_string());

        assert_eq!(versionator.get_phase(), "beta")
    }

    #[test]
    fn get_phase_alpha() {
        let versionator =
            Versionator::new("0.0.0-2-g1a9ea6f730a2c".to_string(), "develop".to_string());

        assert_eq!(versionator.get_phase(), "alpha")
    }

    #[test]
    fn get_phase_alpha_dev() {
        let versionator = Versionator::new(
            "0.0.0-2-g1a9ea6f730a2c".to_string(),
            "feature/testingPhases".to_string(),
        );

        assert_eq!(versionator.get_phase(), "alpha-testingPhases")
    }

    #[test]
    #[should_panic]
    fn get_phase_alpha_dev_sparkle() {
        let versionator = Versionator::new(
            "0.0.0-2-g1a9ea6f730a2c".to_string(),
            "feature/✨".to_string(),
        );

        assert_eq!(versionator.get_phase(), "alpha-✨")
    }

    #[test]
    #[should_panic]
    fn get_phase_invalid_branch() {
        let versionator = Versionator::new(
            "0.0.0-2-g1a9ea6f730a2c".to_string(),
            "heyThisIsNotAValidBranchForVIT".to_string(),
        );

        assert_eq!(versionator.get_phase(), "heyThisIsNotAValidBranchForVIT")
    }

    #[test]
    fn get_next_version() {
        let old_version = "0.0.1";
        let (next_version, last_patch) = Versionator::get_next_version(old_version);

        assert_eq!(next_version, "0.1.0");
        assert_eq!(last_patch, "1");
    }

    #[test]
    fn get_version_id_beta() {
        let versionator =
            Versionator::new("0.0.0-2-g1a9ea6f730a2c".to_string(), "main".to_string());

        assert_eq!(versionator.get_version_id(), "0.1.0-beta.0.2")
    }

    #[test]
    fn get_feature_id_gitlab_style() {
        let feature_id = Versionator::get_feature_id("3-extend-parsing");

        assert_eq!(feature_id, "3")
    }

    #[test]
    fn get_feature_id_gitlab_style_weird_but_valid() {
        let feature_id = Versionator::get_feature_id("5385983475-3-extend-parsing");

        assert_eq!(feature_id, "5385983475")
    }

    #[test]
    fn get_feature_id_feature_branch_with_number() {
        let feature_id = Versionator::get_feature_id("feature/3");

        assert_eq!(feature_id, "3")
    }

    #[test]
    fn get_feature_id_feature_branch_with_number_and_sub() {
        let feature_id = Versionator::get_feature_id("feature/3/extendParsing");

        assert_eq!(feature_id, "3")
    }

    #[test]
    fn get_feature_id_feature_branch_with_id() {
        let feature_id = Versionator::get_feature_id("feature/DEVOPS-3");

        assert_eq!(feature_id, "DEVOPS-3")
    }

    #[test]
    fn get_feature_id_feature_branch_with_id_and_sub() {
        let feature_id = Versionator::get_feature_id("feature/DEVOPS-3/extendParsing");

        assert_eq!(feature_id, "DEVOPS-3")
    }

    #[test]
    fn get_feature_id_feature_branch_with_name() {
        let feature_id = Versionator::get_feature_id("feature/extendParsing");

        assert_eq!(feature_id, "extendParsing")
    }

    #[test]
    fn get_feature_id_jira() {
        let feature_id = Versionator::get_feature_id("DEVOPS-3");

        assert_eq!(feature_id, "DEVOPS-3")
    }

    #[test]
    fn get_feature_id_jira_with_sub() {
        let feature_id = Versionator::get_feature_id("DEVOPS-3/extendParsing");

        assert_eq!(feature_id, "DEVOPS-3")
    }
}
