mod versionator;
use crate::versionator::Versionator;

mod git_adapter;
use crate::git_adapter::GitAdapter;

fn main() {
    let git_adapter = GitAdapter::new(".");

    let branch = git_adapter.get_current_branch();
    let descriptor = git_adapter.get_git_description();

    let versionator = Versionator::new(descriptor, branch.clone());

    println!(
        "{} {} {}",
        versionator.get_version_id(),
        versionator.get_version_id_short(),
        branch
    );
}
