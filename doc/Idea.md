# Idea
This is the original Idea, but to see the current behavior, just look at the `README.md`.

# Input
- Executed in Git repo
    - Path of Git repo


# Output
## Default

```
<VERSION_ID> <FANCY_VERSION_ID>
```

```mermaid
sequenceDiagram
    participant m as main
    participant d as develop
    participant f as feature/✨
    
    m ->> m: tag
    Note left of m: 1.0.0 <br> stable
    m -) d: checkout
    Note left of d: 1.1.0-alpha-0 <br> alpha
    d -) f: checkout
    Note left of f: 1.1.0-alpha-dev-✨-0
    f -->> f: Add ⭐
    Note left of f: 1.1.0-alpha-dev-✨-1
    f -->> f: Add 2
    Note left of f: 1.1.0-alpha-dev-✨-2
    f ->> d: merge
    Note left of d: 1.1.0-alpha-3 <br> alpha
    d ->> m: merge
    Note left of m: 1.1.0-beta-4 <br> beta
    m ->> m: tag
    Note left of m: 1.1.0 <br> stable
```


# Features
- Support CI
- Support local build
